#!/usr/bin/perl
use Cwd;
use strict;
use warnings;

my $startDir = getcwd;
my $success = 0;
my $allTries = 0;
my $path = $ARGV[0];
chdir $path;
chdir "eml_files_sorted";

foreach my $category (`ls`) {
	chomp $category;
	chdir $category;
	foreach my $message (`ls`) {
		chomp $message;
		if (runPrototype($message) eq $category) {
			print "got $message!\n";
			$success += 1;
		} else {
			print "missed $message.\n";
		}
		$allTries += 1;
	}
	chdir "..";
}
chdir "..";
print successPercent()."%\n";

sub successPercent {
	return $success / $allTries * 100;
}

sub runPrototype {
	my $dir = getcwd;
	my $result;
	`mv "$_[0]" "../../Inbox"`;
	chdir $startDir;
	my $i = `java -jar prototype-core-all-0.1-SNAPSHOT.jar ./`; # 2>/dev/null `;
	if ( $i =~ /Категория: (.+)/ ) { 
		$result = $1; 
	}
	chdir $dir;
	`"mv ../../Inbox/$_[0] ./"`;
	return $result;
}

