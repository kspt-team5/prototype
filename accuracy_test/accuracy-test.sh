#!/bin/bash

startDir=$(pwd);
path=$1;
success=0;
allTries=0;

successPercent () {
	echo $( echo "scale=6;$success/$allTries*100" | bc )%;
}

runPrototype () {
	dir=$( pwd );
	mv "$1" "../../Inbox";
	cd "$startDir";
	i=$( java -jar prototype-core-all-0.1-SNAPSHOT.jar ./ 2>/dev/null );
	echo $i | sed -n 's/Категория: \([a-zA-Zа-яА-Я]\+\).*/\1/p';
	cd "$dir";
	mv "../../Inbox/$1" "./";
}

cd "$path";
cd "eml_files_sorted";
for category in $( ls ); do
	cd "$category";
	for message in $( ls ); do
		tmp=$( runPrototype $message );
		echo $tmp;
		if [[ "$tmp" == "$category" ]]; then
			printf "got $message!\n";
			((++success));
		else
			printf "missed $message.\n";
		fi
		((++allTries));
	done
	cd "..";
done
cd "..";
successPercent;
