import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class TestEmail {
    private final Path path = Paths.get(System.getProperty("user.dir"));
    private final Path pathToCategories = path.resolve("eml_files_sorted");
    private final Path pathToInbox = path.resolve("Inbox");

    @Test
    public void main() {
        Prototype prototype = new Prototype();
        prototype.setPathToCategories(pathToCategories);
        prototype.setPathToInbox(pathToInbox);
        prototype.doTheBusiness();
    }
}
