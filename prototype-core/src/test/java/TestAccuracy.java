import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TestAccuracy {

    private Path startPath = Paths.get(System.getProperty("user.dir"));
    private Path sortedPath = Paths.get(startPath.toString() + File.separator + "eml_files_sorted");
    private Path inboxPath = Paths.get(startPath.toString() + File.separator + "Inbox");
    private String fileRealParent;
    private double success;
    private double allTries;

    @Test
    public void TestAccuracy() {
        for (File category : new File(sortedPath.toString()).listFiles())
            if (category.isDirectory())
                for (File message : category.listFiles())
                    if (message.isFile())
                        performTest(message);
        System.out.println("Success rate = " + (success / allTries * 100d) + "%");
    }

    private void performTest(File message) {
        pickMessage(message);
        runAndCheck(message);
        putMessageBack();
    }

    private void pickMessage(File message) {
        fileRealParent = message.getParentFile().getName();
        message.renameTo(new File(inboxPath.toString() + File.separator + message.getName()));
    }

    private void runAndCheck(File message) {
        String result = runPrototype();
        if((result).equals(fileRealParent)) {
            System.out.println("+ " + result + ": " + message.getName());
            success += 1d;
        } else {
            System.out.println("- " + result + " (" + fileRealParent + "): " + message.getName());
        }
        allTries += 1d;
    }

    private void putMessageBack() {
        for (File message : new File(inboxPath.toString()).listFiles())
            message.renameTo(new File(sortedPath.toString() + File.separator + fileRealParent + File.separator + message.getName()));
    }

    private String runPrototype() {
        Prototype_old prototype = new Prototype_old();
        prototype.setPathToCategories(sortedPath);
        prototype.setPathToInbox(inboxPath);
        prototype.doTheBusiness();
        return prototype.getResultCategory();
    }
}
