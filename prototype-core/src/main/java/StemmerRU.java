import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StemmerRU {
    private static final Pattern NOISEWORDSRU = Pattern.compile("^(-|еще|него|сказать|а|ж|нее|со|без|же|ней|совсем|более|жизнь|нельзя|так|больше|за|нет|такой|будет|зачем|ни|там|будто|здесь|нибудь|тебя|бы|и|никогда|тем|был|из|ним|теперь|была|из-за|них|то|были|или|ничего|тогда|было|им|но|того|быть|иногда|ну|тоже|в|их|о|только|вам|к|об|том|вас|кажется|один|тот|вдруг|как|он|три|ведь|какая|она|тут|во|какой|они|ты|вот|когда|опять|у|впрочем|конечно|от|уж|все|которого|перед|уже|всегда|которые|по|хорошо|всего|кто|под|хоть|всех|куда|после|чего|всю|ли|потом|человек|вы|лучше|потому|чем|г|между|почти|через|где|меня|при|что|говорил|мне|про|чтоб|да|много|раз|чтобы|даже|может|разве|чуть|два|можно|с|эти|для|мой|сам|этого|до|моя|свое|этой|другой|мы|свою|этом|его|на|себе|этот|ее|над|себя|эту|ей|надо|сегодня|я|ему|наконец|сейчас|если|нас|сказал|есть|не|сказала)$");

    private static final Pattern NOISEWORDSEN = Pattern.compile("(a|about|after|all|also|an|another|any|are|as|and|at|be|because|been|before|being|between|but|both|by|came|can|come|could|did|do|teach|even|for|from|further|furthermore|get|got|has|had|he|have|her|here|him|himself|his|how|hi|however|i|if|in|into|is|it|its|indeed|just|like|made|many|me|might|more|moreover|most|much|must|my|never|not|now|of|on|only|other|our|out|or|over|said|same|see|should|since|she|some|still|such|take|than|that|the|their|them|then|there|these|therefore|they|this|those|through|to|too|thus|under|up|very|was|way|we|well|were|what|when|where|which|while|who|will|with|would|you|your|nbsp)$");

    private static final Pattern PERFECTIVEGROUND = Pattern.compile("((ив|ивши|ившись|ыв|ывши|ывшись)|(([ая])(в|вши|вшись)))$");

    private static final Pattern REFLEXIVE = Pattern.compile("(с[яь])$");

    private static final Pattern ADJECTIVE = Pattern.compile("(ее|ие|ые|ое|ими|ыми|ей|ий|ый|ой|ем|им|ым|ом|его|ого|ему|ому|их|ых|ую|юю|ая|яя|ою|ею)$");

    private static final Pattern PARTICIPLE = Pattern.compile("((ивш|ывш|ующ)|((?<=[ая])(ем|нн|вш|ющ|щ)))$");

    private static final Pattern VERB = Pattern.compile("((ила|ыла|ена|ейте|уйте|ите|или|ыли|ей|уй|ил|ыл|им|ым|ен|ило|ыло|ено|ят|ует|уют|ит|ыт|ены|ить|ыть|ишь|ую|ю)|((?<=[ая])(ла|на|ете|йте|ли|й|л|ем|н|ло|но|ет|ют|ны|ть|ешь|нно)))$");

    private static final Pattern NOUN = Pattern.compile("(а|ев|ов|ие|ье|е|иями|ями|ами|еи|ии|и|ией|ей|ой|ий|й|иям|ям|ием|ем|ам|ом|о|у|ах|иях|ях|ы|ь|ию|ью|ю|ия|ья|я)$");

    private static final Pattern SlitOnFirstVowel = Pattern.compile("^(.*?[аеиоуыэюя])(.*)$");

    private static final Pattern DERIVATIONAL = Pattern.compile(".*[^аеиоуыэюя]+[аеиоуыэюя].*ость?$");

    private static final Pattern DER = Pattern.compile("ость?$");

    private static final Pattern SUPERLATIVE = Pattern.compile("(ейше|ейш)$");

    private static final Pattern I = Pattern.compile("и$");
    private static final Pattern SOFTSIGN = Pattern.compile("ь$");
    private static final Pattern NN = Pattern.compile("нн$");

    public static String removeNoiseWord(String word) {
        word = prepare(word);
        word = NOISEWORDSRU.matcher(word).replaceFirst("");
        word = NOISEWORDSEN.matcher(word).replaceFirst("");
        return word;
    }

    public static String stem(String word) {
        word = prepare(word);
        word = doPorterStemming(word);
        return word;
    }

    private static String prepare(String word) {
        word = word.toLowerCase();
        word = word.replace('\u0451','\u0435');
        return word;
    }

    private static String doPorterStemming(String word) {
        Matcher m = SlitOnFirstVowel.matcher(word);
        if (m.matches())
            word = stripSuffixes(m);
        return word;
    }

    private static String stripSuffixes(Matcher m) {
        String pre = m.group(1);
        String remain = m.group(2);
        remain = stripInflectionalSuffix(remain);
        remain = stripI(remain);
        remain = stripDerivationalSuffix(remain);
        String temp = SOFTSIGN.matcher(remain).replaceFirst("");
        if (temp.equals(remain)) {
            remain = SUPERLATIVE.matcher(remain).replaceFirst("");
            remain = NN.matcher(remain).replaceFirst("н");
        } else {
            remain = temp;
        }
        return pre + remain;
    }

    private static String stripInflectionalSuffix(String remain) {
        String temp = PERFECTIVEGROUND.matcher(remain).replaceFirst("");
        if (temp.equals(remain)) {
            remain = REFLEXIVE.matcher(remain).replaceFirst("");
            temp = ADJECTIVE.matcher(remain).replaceFirst("");
            if (!temp.equals(remain)) {
                remain = temp;
                remain = PARTICIPLE.matcher(remain).replaceFirst("");
            } else {
                temp = VERB.matcher(remain).replaceFirst("");
                if (temp.equals(remain))
                    remain = NOUN.matcher(remain).replaceFirst("");
                else
                    remain = temp;
            }
        } else {
            remain = temp;
        }
        return remain;
    }

    private static String stripDerivationalSuffix(String remain) {
        if (DERIVATIONAL.matcher(remain).matches())
            return DER.matcher(remain).replaceFirst("");
        return remain;
    }

    private static String stripI(String remain) {
        return I.matcher(remain).replaceFirst("");
    }
}
