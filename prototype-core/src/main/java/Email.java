import com.fasterxml.jackson.databind.ObjectMapper;
import edu.stanford.nlp.process.StemmerEN;
import lombok.Getter;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Getter
public class Email {
    private String address;
    private String subject;
    private String content;
    private String[] fwords;

    public Email(String filename) {
        HashMap messageJsonFile = new HashMap<>();
        try {
            messageJsonFile = loadMessageJsonFile(filename);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error loading file: " + filename);
            System.exit(-1);
        }
        readTheFields(messageJsonFile);
        fillFwords();
    }

    private HashMap loadMessageJsonFile(String filename) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        HashMap jsonEMLFile = new HashMap();
        jsonEMLFile = mapper.readValue(new File(filename), jsonEMLFile.getClass());
        return jsonEMLFile;
    }

    private void readTheFields(HashMap<String, ArrayList<HashMap<String, String>>> messageJsonFile) {
        this.address = messageJsonFile.get("from").get(0).get("email");
        this.subject = String.valueOf(messageJsonFile.get("subject"));
        this.content = "";
        for (int i = 0; i < messageJsonFile.get("attachments").size(); i++) {
            if (messageJsonFile.get("attachments").get(i).get("content_type").equals("text/plain") ||
                    messageJsonFile.get("attachments").get(i).get("content_type").equals("text/html"))
            this.content += messageJsonFile.get("attachments").get(i).get("content");
        }
        for (int i = 0; i < messageJsonFile.get("parts").size(); i++) {
            this.content += messageJsonFile.get("parts").get(i).get("content");
        }
    }

    private void fillFwords() {
        stripGarbage();
        this.fwords = this.content.toLowerCase().split("[^a-zA-Zа-яА-Я]");
        removeEmptyWords();
        stemRuEn();
        removeEmptyWords();
    }

    private void stripGarbage() {
        stripHtmlAndCss();
        stripURLs();
        stripDigits();
    }

    private void stemRuEn() {
        StemmerEN stemmer = new StemmerEN();
        for (int i = 0; i < this.fwords.length; ++i) {
            fwords[i] = StemmerRU.removeNoiseWord(fwords[i]);
            fwords[i] = StemmerRU.stem(fwords[i]);
            fwords[i] = stemmer.stem(fwords[i]);
        }
    }

    private void removeEmptyWords() {
        List<String> list = new ArrayList<>();
        for (String fword : fwords) {
            if (fword != null && fword.length() > 0)
                list.add(fword);
        }
        fwords = list.toArray(new String[list.size()]);
    }

    private void stripHtmlAndCss() {
        content = Jsoup.clean(content, Whitelist.none());
    }

    private void stripURLs() {
        String regExpURL = "(((http|ftp|https):\\/\\/)?[\\w\\-_]+(\\.[\\w\\-_]+)+([\\w\\-\\.,@?^=%&amp;:/~\\+#]*[\\w\\-\\@?^=%&amp;/~\\+#])?)";
        this.content = this.content.replaceAll(regExpURL, "");
        this.subject = this.subject.replaceAll(regExpURL, "");
    }

    private void stripDigits() {
        this.content = this.content.replaceAll("[0-9]", "");
        this.subject = this.subject.replaceAll("[0-9]", "");
    }
}