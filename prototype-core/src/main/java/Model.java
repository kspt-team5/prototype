import Jama.Matrix;
import Jama.SingularValueDecomposition;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;

public class Model {
    private ArrayList<Email> emailList;
    private HashMap<String, ArrayList<Email>> categoryList;
    private final int categorySize = 0;

    private double[][] matrix;
    private String[] terms;

    private double[][] termsMatrix;
    private double[][] emailsMatrix;
    private int dimension;

    public Model(HashMap<String, ArrayList<Email>> categoryList){
        this.categoryList = categoryList;
        this.emailList = new ArrayList<>();
        for (String key : categoryList.keySet()) {
            ArrayList<Email> tempList = categoryList.get(key);
            for (int i = 0; i < tempList.size(); ++i){
                emailList.add(tempList.get(i));
            }
        }
        this.update();
    }


    //MAKE SOME DICTIONARY
    private void buildFrequencyMatrix() {
        ArrayList<String> words = readAllTheWords();
        fillTerms(words);
        fillMatrix(words);
    }
    private ArrayList<String> readAllTheWords() {
        ArrayList<String> words = new ArrayList<>();
        Iterator<Email> iter = emailList.iterator();
        while (iter.hasNext()) {
            Email email = iter.next();
            for (int i = 0; i < email.getFwords().length; ++i)
                if (!words.contains(email.getFwords()[i]))
                    words.add(email.getFwords()[i]);
        }
        return words;
    }
    private void fillTerms(ArrayList<String> words) {
        terms = new String[words.size()];
        words.toArray(terms);
    }
    private void fillMatrix(ArrayList<String> words) {
        matrix = new double[terms.length][emailList.size()];
        for (int j = 0; j < emailList.size(); ++j) {
            String[] fwords = emailList.get(j).getFwords();
            for (int i = 0; i < fwords.length; ++i)
                if (words.contains(fwords[i]))
                    ++matrix[words.indexOf(fwords[i])][j];
        }
    }
    private void removeMinorTerms() {
        //Удаление лишних строк
        Integer len = 0;
        for (int t = 0; t < terms.length; ++t) {
            int flag = 0;
            for (int i = 0; i < emailList.size(); ++i)
                if (matrix[t][i] > 0.0)
                    flag++;
            if (flag < 2)
                terms[t] = "";
            else len++;
        }
        double[][] rematrix = new double[len][emailList.size()];
        String[] temp = new String[len];
        for (int t = 0, i = 0; t < terms.length; ++t) {
            if (terms[t] != "") {
                for (int e = 0; e < emailList.size(); ++e)
                    rematrix[i][e] = matrix[t][e];
                temp[i] = terms[t];
                i++;
            }
        }
        terms = temp;
        matrix = rematrix;
    }
    private void singularValueDecomposition() {
        Matrix M = new Matrix(matrix);
        SingularValueDecomposition SVD = new SingularValueDecomposition(M);
        Matrix V = SVD.getV();
        Matrix U = SVD.getU();
        //Matrix S = SVD.getS();

        double[][] tMatrix = new double[this.terms.length][dimension];
        for (int t = 0; t < this.terms.length; t++){
            for (int k = 0; k < dimension; k++){
                tMatrix[t][k] = U.get(t,k);
            }
        }

        double[][] eMatrix = new double[emailList.size()][dimension];
        for (int k = 0; k < dimension; k++){
            for (int e = 0; e < this.emailList.size(); e++){
                eMatrix[e][k] = V.get(e,k);
            }
        }
        this.emailsMatrix = eMatrix;
        this.termsMatrix = tMatrix;
    }
    private int findSpaceDimension() {
        if (emailList.size() < 10)
            return 2;
        if (emailList.size() < 100)
            return (int) (emailList.size() / 10.0);
        else if (emailList.size() < 1000)
            return (int) (emailList.size() / 20.0);
        else if (emailList.size() < 10000)
            return (int) (emailList.size() / 100.0);
        else return (int) (emailList.size() / 1000.0);
    }

    public void update() {
        buildFrequencyMatrix();
        removeMinorTerms();
        dimension = findSpaceDimension();
        singularValueDecomposition();
    }

    //CLASSIFIER
    private String classification(Email email) {
        double[] emailVector = this.getVector(email);
        HashMap<String, Double> weightsEmailFromCategories = new HashMap<String, Double>();
        for (String key : categoryList.keySet()) {
            if (isCategoryReady(key)) {
                ArrayList<Email> emailsFromCategory = categoryList.get(key);
                ArrayList<double[]> vectorsFromCategory = new ArrayList<double[]>();
                //Получили векторы документов в категории
                for (Email e : emailsFromCategory)
                    vectorsFromCategory.add(this.getVector(e));

                //Нашли минимальное значение оценки
                double[][] values = new double[emailsFromCategory.size()][emailsFromCategory.size()];
                double MIN = 1.0;
                for (int i = 0; i < emailsFromCategory.size(); ++i) {
                    for (int j = i + 1; j < emailsFromCategory.size(); ++j) {
                        values[i][j] = calculateCosWeight(vectorsFromCategory.get(i), vectorsFromCategory.get(j));
                        if (values[i][j] < MIN)
                            MIN = values[i][j];
                    }
                }

                //Мат. ожидание
                double expectedValue = 0.0, dispersion = 0.0;
                double[] weigths = new double[emailsFromCategory.size()];
                for (int i = 0; i < weigths.length; ++i) {
                    weigths[i] = calculateCosWeight(emailVector, vectorsFromCategory.get(i));
                    expectedValue += weigths[i];
                }
                expectedValue = expectedValue / weigths.length;
                //СКО
                for (int i = 0; i < weigths.length; ++i) {
                    dispersion += Math.pow(weigths[i] - expectedValue, 2);
                }
                dispersion = dispersion / weigths.length;
                if (MIN - Math.sqrt(dispersion) <= expectedValue)
                    weightsEmailFromCategories.put(key, expectedValue);
                else weightsEmailFromCategories.put(key, -1.0);
            }
        }
        //Сравнение полученных оценок
        String cat = "";
        double max = -1.0;
        for (String key : categoryList.keySet()) {
            if (weightsEmailFromCategories.get(key) > max) {
                max = weightsEmailFromCategories.get(key);
                cat = key;
            }
        }
        if (max != -1.0 && cat != ""){
            this.addEmailToCategory(email, cat);
            return cat;
        } else {
            return "INVALID_CATEGORY";
        }
    }
    private double[] getVector(Email email){
        double[] vector = new double[dimension];
        int index = emailList.indexOf(email);
        for (int k = 0; k < dimension; ++k) {
            vector[k] = emailsMatrix[index][k];
        }
        return vector;
    }
    private double calculateCosWeight(double v1[], double[] v2){
        double result = 0, normV1 = 0, normV2 = 0;
        for (int i = 0; i < v1.length; i++){
            result += v1[i]*v2[i];
            normV1 += Math.pow(v1[i], 2);
            normV2 += Math.pow(v2[i], 2);
        }
        return result/(Math.sqrt(normV1)*Math.sqrt(normV2));
    }


    //KAKAYA-TO DICH
    private void addEmailToModel(Email email) {
        if (!isBelongsToModel(email)) {
            this.emailList.add(email);
            update();
        }
    }
    private void addEmailToCategory(Email email, String category){
        if (isBelongsToModel(email) && isBelongsToModel(category) && !isEmailBelongsAnyCategory(email)) {
            ArrayList<Email> emailList = categoryList.get(category);
            if (!emailList.contains(email)) {
                emailList.add(email);
                categoryList.put(category, emailList);
            }
        }
    }
    public String classifyEmail(Email email){
        this.addEmailToModel(email);
        return this.classification(email);
    }
    private boolean isEmailBelongsAnyCategory(Email email){
        if (emailList.contains(email)){
            for (String key : categoryList.keySet())
                if (categoryList.get(key).contains(email))
                    return true;
        }
        return false;
    }
    private boolean isCategoryEmpty(String category){
        if (categoryList.containsKey(category) && categoryList.get(category).isEmpty())
            return true;
        return false;
    }
    private boolean isCategoryReady(String category){
        if (categoryList.get(category).size() < categorySize)
            return false;
        return true;
    }
    private boolean isBelongsToModel(String category){
        if (categoryList.containsKey(category))
            return true;
        return false;
    }
    private boolean isBelongsToModel(Email email){
        if (emailList.contains(email))
            return true;
        return false;
    }
}
