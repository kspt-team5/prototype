import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;

public class Prototype_old {
    @Setter
    private Path pathToCategories;
    @Setter
    private Path pathToInbox;
    private HashMap<String, HashMap<String, Double>> weightsOfCategorized = new HashMap<>();
    private HashMap<String, Double> weightsOfIncomingMsg = new HashMap<>();
    @Getter
    private String resultCategory;

    public void doTheBusiness() {
        processBothCategorizedAndInbox();
        classifyIncomingMsg();
    }

    private void processBothCategorizedAndInbox() {
        processCategorized();
        processIncomingMsg();
        normalizeWeights();
    }

    private void classifyIncomingMsg() {
        HashMap<String, Double> result = calculateCategoryParticipation();
        resultCategory  = chooseMostProbableCategory(result);
    }

    private void processCategorized() {
        final HashMap<String, ArrayList<Email>> categories = loadCategorized();
        HashMap<String, LSA> lsaCategories = new HashMap<>();
        categories.forEach((category, messages) -> {
            lsaCategories.put(category, new LSA(messages));
            lsaCategories.get(category).calculate();
            HashMap<String, Double> weightHashMap = calculateWeights(lsaCategories.get(category));
            weightsOfCategorized.put(category, weightHashMap);
        });
    }

    private void processIncomingMsg() {
        ArrayList<Email> inbox = loadMessages(pathToInbox);
        LSA lsaForIncomingMsg = new LSA(inbox);
        lsaForIncomingMsg.calculateIncomingMsg();
        String[] terms = lsaForIncomingMsg.getTerms();
        double[][] arr = lsaForIncomingMsg.getMatrix();
        for (int j = 0; j < terms.length; ++j)
            weightsOfIncomingMsg.put(terms[j], arr[j][0]);
    }

    private void normalizeWeights() {
        normalizeWeightsOfCategorized();
        normalizeWeightsOfIncomingMsg();
    }

    private HashMap<String, Double> calculateCategoryParticipation() {
        HashMap<String, Double> result = new HashMap<>();
        weightsOfCategorized.forEach((categoryName, wordWeights) -> {
            Double scoreForThisCategory = weightsOfIncomingMsg.entrySet().stream().mapToDouble(
                    word -> word.getValue() * wordWeights.getOrDefault(word.getKey(), 0d)
            ).sum();
            result.put(categoryName, scoreForThisCategory);
        });
        return result;
    }

    private String chooseMostProbableCategory(HashMap<String, Double> result) {
        String biggestName = "";
        Double biggestValue = 0d;
        for (String key : result.keySet())
            if (result.get(key) > biggestValue) {
                biggestValue = result.get(key);
                biggestName = key;
            }
        return biggestName;
    }

    private HashMap<String, ArrayList<Email>> loadCategorized() {
        HashMap<String, ArrayList<Email>> categories = new HashMap<>();
        for (File folder : new File(pathToCategories.toString()).listFiles()) {
            if (folder.isDirectory() && !folder.getName().equals("Uncategorized")) {
                categories.put(folder.getName(), loadMessages(pathToCategories.resolve(folder.getPath())));
            }
        }
        return categories;
    }

    private HashMap<String, Double> calculateWeights(LSA lsa) {
        HashMap<String, Double> weightHash = new HashMap<>();
        for (int j = 0; j < lsa.getTerms().length; ++j) {
            Double weight = 0d;
            for (int i = 0; i < lsa.getEmailList().size(); ++i)
                weight += lsa.getMatrix()[j][i];
            weightHash.put(lsa.getTerms()[j], weight);
        }
        return weightHash;
    }

    private ArrayList<Email> loadMessages(Path path) {
        ArrayList<Email> emails = new ArrayList<>();
        for (File file : new File(path.toString()).listFiles())
            if (file.isFile())
                emails.add(new Email(path.resolve(file.getPath()).toString()));
        return emails;
    }

    private void normalizeWeightsOfCategorized() {
        weightsOfCategorized.forEach((key, value) -> {
            normalizeWeights(value);
        });
    }

    private void normalizeWeightsOfIncomingMsg() {
        normalizeWeights(weightsOfIncomingMsg);
    }

    private void normalizeWeights(HashMap<String, Double> weights) {
        Double sum = weights.values().stream().mapToDouble(a -> a).sum();
        weights.forEach((term, weight) -> {
            weights.put(term, weight / sum);
        });
    }
}
