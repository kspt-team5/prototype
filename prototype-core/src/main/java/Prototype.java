import lombok.Setter;

import java.io.File;
import java.nio.file.Path;
import java.util.*;

public class Prototype {
    @Setter
    private Path pathToCategories;
    @Setter
    private Path pathToInbox;
    private String resultCategory;

    public String getResultCategory() {
        return resultCategory;
    }

    public void doTheBusiness() {
        HashMap<String, ArrayList<Email>> categories;
        categories = loadCategorized();
        Model model = new Model(categories);
        ArrayList<Email> inbox = loadMessages(pathToInbox);
        resultCategory = model.classifyEmail(inbox.get(0));
    }

    private HashMap<String, ArrayList<Email>> loadCategorized() {
        HashMap<String, ArrayList<Email>> categories = new HashMap<>();
        for (File folder : new File(pathToCategories.toString()).listFiles()) {
            if (folder.isDirectory() && !folder.getName().equals("Uncategorized")) {
                categories.put(folder.getName(), loadMessages(pathToCategories.resolve(folder.getPath())));
            }
        }
        return categories;
    }

    private ArrayList<Email> loadMessages(Path path) {
        ArrayList<Email> emails = new ArrayList<>();
        for (File file : new File(path.toString()).listFiles())
            if (file.isFile())
                emails.add(new Email(path.resolve(file.getPath()).toString()));
        return emails;
    }
}
