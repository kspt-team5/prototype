import java.nio.file.Path;
import java.nio.file.Paths;

public class main {

    public static void main(String[] args) {
        Path path = Paths.get(args[0]);
        Path pathToCategories = path.resolve("eml_files_sorted");
        Path pathToInbox = path.resolve("Inbox");
        Prototype_old prototype = new Prototype_old();
        prototype.setPathToCategories(pathToCategories);
        prototype.setPathToInbox(pathToInbox);
        prototype.doTheBusiness();
    }
}
