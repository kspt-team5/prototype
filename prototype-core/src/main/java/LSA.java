import lombok.Getter;

import java.util.ArrayList;
import java.util.Iterator;

@Getter
public class LSA {
    private ArrayList<Email> emailList;
    private double[][] matrix;
    private String[] terms;

    public LSA(ArrayList<Email> emailList) {
        this.emailList = emailList;
    }

    public void calculate() {
        buildFrequencyMatrix();
        removeMinorTerms();
    }

    public void calculateIncomingMsg() {
        buildFrequencyMatrix();
    }

    private void buildFrequencyMatrix() {
        ArrayList<String> words = readAllTheWords();
        fillTerms(words);
        fillMatrix(words);
    }

    private void removeMinorTerms() {
        //Удаление лишних строк
        Integer len = 0;
        for (int t = 0; t < terms.length; ++t) {
            int flag = 0;
            for (int i = 0; i < emailList.size(); ++i)
                if (matrix[t][i] > 0.0)
                    flag++;
            if (flag < 2)
                terms[t] = "";
            else len++;
        }
        double[][] rematrix = new double[len][emailList.size()];
        String[] temp = new String[len];
        for (int t = 0, i = 0; t < terms.length; ++t) {
            if (terms[t] != "") {
                for (int e = 0; e < emailList.size(); ++e)
                    rematrix[i][e] = matrix[t][e];
                temp[i] = terms[t];
                i++;
            }
        }
        terms = temp;
        matrix = rematrix;
    }

    private ArrayList<String> readAllTheWords() {
        ArrayList<String> words = new ArrayList<>();
        Iterator<Email> iter = emailList.iterator();
        while (iter.hasNext()) {
            Email email = iter.next();
            for (int i = 0; i < email.getFwords().length; ++i)
                if (!words.contains(email.getFwords()[i]))
                    words.add(email.getFwords()[i]);
        }
        return words;
    }

    private void fillTerms(ArrayList<String> words) {
        terms = new String[words.size()];
        words.toArray(terms);
    }

    private void fillMatrix(ArrayList<String> words) {
        matrix = new double[terms.length][emailList.size()];
        for (int j = 0; j < emailList.size(); ++j) {
            String[] fwords = emailList.get(j).getFwords();
            for (int i = 0; i < fwords.length; ++i)
                if (words.contains(fwords[i]))
                    ++matrix[words.indexOf(fwords[i])][j];
        }
    }

}
